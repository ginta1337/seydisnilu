package ru.aksoftware.seydisnilu.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

import ru.aksoftware.seydisnilu.R;
import ru.aksoftware.seydisnilu.game.BarabanContent;
import ru.aksoftware.seydisnilu.game.Game;
import ru.aksoftware.seydisnilu.misc.Helper3D;

public class Baraban {

    private final float heigth;
    private final int sections;
    private final float radius;
    private final BarabanContent content;
    private final Game game;
    private float[] bottomCenterPoint;
    private float[] vertices;
    public static final float PI = 3.1415f;

    private FloatBuffer vertexBufferSides;
    private FloatBuffer vertexBufferTop;
//    private FloatBuffer vertexBufferBottom;

    //private FloatBuffer[]  sectorBuffers;

    private ShortBuffer indexBuffer;

    private float[] centerPoint;
    private float[] textureCoords;
    private FloatBuffer uvBuffer;
    private int[] textureNames;
    private ShortBuffer[] sectorsIndexes;

    public int actualSections;
    private int pointsPerSector;


    public Baraban(int sections, float heigth, float radius, Game game) {
        this.sections = sections;
        this.heigth = heigth;
        this.radius = radius;
        this.content = game.getBarabanContent();
        this.game = game;
        calcuatePoints();

        prepareBuffers();

    }

    public void prepareTextures(GL10 gl, Context mContext) {
        //Generate textures
        textureNames = new int[1];
        gl.glGenTextures(1, textureNames, 0);


        Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.tex_sey1);
        // Bind texture to texturename
        gl.glActiveTexture(GL10.GL_TEXTURE0);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureNames[0]);


        // create nearest filtered texture
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);


        // Load the bitmap into the bound texture.
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bmp, 0);

        // We are done using the bitmap so we should recycle it.
        bmp.recycle();
    }

    private void prepareBuffers() {


        vertexBufferSides = Helper3D.arrayToBuffer(vertices);


        vertexBufferTop = prepareFlatSide(false);
        //vertexBufferBottom = prepareFlatSide(true);

        // short is 2 bytes, therefore we multiply the number if
        // vertices with 2.
        indexBuffer = Helper3D.createSequentialBuffer((actualSections + 2) * 2);


        uvBuffer = Helper3D.arrayToBuffer(textureCoords);

        sectorsIndexes = new ShortBuffer[content.size()];
        for(int i = 0; i < content.size(); i++) {
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect((pointsPerSector + 2) * 2);
            byteBuffer.order(ByteOrder.nativeOrder());
            sectorsIndexes[i] = byteBuffer.asShortBuffer();
            sectorsIndexes[i].put((short) 0);
            for(int j = 0; j <= pointsPerSector; j++ ) {
                sectorsIndexes[i].put((short) (1 + j + i*pointsPerSector));
            }
            sectorsIndexes[i].position(0);
        }

    }

    private FloatBuffer prepareFlatSide(boolean bottom) {
        ByteBuffer topBB = ByteBuffer.allocateDirect((actualSections + 2) * 3 * 4);
        topBB.order(ByteOrder.nativeOrder());
        FloatBuffer floatBuffer = topBB.asFloatBuffer();

        floatBuffer.put(bottom ? bottomCenterPoint : centerPoint, 0, 3);
        for (int i = 0; i < actualSections; i++) {
            int verticeIndex = i * 2 * 3 + (bottom ? 0 : 3);
            floatBuffer.put(vertices, verticeIndex, 3);
        }
        floatBuffer.put(vertices, (bottom ? 0 : 3), 3);
        floatBuffer.position(0);
        return floatBuffer;
    }

    private void calcuatePoints() {
        int ost = sections % content.size();
        actualSections = sections + (ost == 0 ? 0 :(content.size() - ost));
        pointsPerSector = actualSections / content.size();
        centerPoint = new float[]{0, 0, heigth};
        bottomCenterPoint = new float[]{0, 0, 0};

        int vertexCount = (actualSections + 1) * 2;
        vertices = new float[vertexCount * 3];
        float alpha = (2.0f * PI) / (float) actualSections;
        for (int i = 0; i < actualSections + 1; i++) {
            float x = (float) Math.cos(alpha * i) * radius;
            float y = (float) Math.sin(alpha * i) * radius;
            int verticeIndex = i * 2 * 3;

            vertices[verticeIndex] = x;
            vertices[verticeIndex + 1] = y;
            vertices[verticeIndex + 2] = 0;
            vertices[verticeIndex + 3] = x;
            vertices[verticeIndex + 4] = y;
            vertices[verticeIndex + 5] = heigth;
        }

        textureCoords = new float[vertexCount * 2];
        float delta = 1.0f / actualSections;
        for (int i = 0; i <= actualSections; i++) {
            textureCoords[4 * i] = delta * i;
            textureCoords[4 * i + 1] = 1;
            textureCoords[4 * i + 2] = delta * i;
            textureCoords[4 * i + 3] = 0;
        }

    }

    public void draw(GL10 gl) {

        gl.glFrontFace(GL10.GL_CCW);
        gl.glDisable(GL10.GL_CULL_FACE);
        // Enable face culling.
        //gl.glEnable(GL10.GL_CULL_FACE);
        // What faces to remove with the face culling.
        //gl.glCullFace(GL10.GL_FRONT_AND_BACK);

        // Enabled the vertices buffer for writing and to be used during
        // rendering.
        drawSides(gl);


        drawSectors(gl);

//        gl.glColor4f(1.0f, 0.0f, 0, 0.8f);
//        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBufferTop);
//        gl.glDrawElements(GL10.GL_TRIANGLE_FAN, actualSections + 2,
//                GL10.GL_UNSIGNED_SHORT, indexBuffer);
//
//        // Disable the vertices buffer.
//        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        // Disable face culling.
        //gl.glDisable(GL10.GL_CULL_FACE);
    }

    private void drawSectors(GL10 gl) {
        for(int i = 0; i < content.size(); i++) {
            if(i == game.getSelectedId()) {
                gl.glColor4f(1.0f, 0.3f, 0.3f, 0.8f);
            } else if(i % 2 == 0) {
                gl.glColor4f(0.0f, 0.0f, 1.0f, 0.8f);
            } else {
                gl.glColor4f(1.0f, 1.0f, 1.0f, 0.8f);
            }
//            gl.glColor4f(1.0f * i / content.size(), 1.0f * i / content.size(), 1.0f, 0.8f);

            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBufferTop);
            gl.glDrawElements(GL10.GL_TRIANGLE_FAN, pointsPerSector + 2,
                    GL10.GL_UNSIGNED_SHORT, sectorsIndexes[i]);
        }

        float sectorAngle = (float) 360 / (float) content.size();
        for(int i = 0; i < content.size(); i++) {


            gl.glPushMatrix();
            gl.glTranslatef(0, 0, heigth + 0.01f);
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glRotatef(sectorAngle/2 + sectorAngle *i, 0, 0, 1);
            content.getSectors()[i].getSectorContent().draw(gl);
            gl.glPopMatrix();
        }
    }

    private void drawSides(GL10 gl) {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        // Specifies the location and data format of an array of vertex
        // coordinates to use when rendering.
        // bind the previously generated texture

        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureNames[0]);

        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glDisable(GL10.GL_COLOR_MATERIAL);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        //gl.glColor4f(0.0f, 0.5f, 0.5f, 0.8f);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 0.0f);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBufferSides);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, uvBuffer);
        gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, (actualSections + 1)*2,
                GL10.GL_UNSIGNED_SHORT, indexBuffer);

        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glEnable(GL10.GL_COLOR_MATERIAL);
        gl.glDisable(GL10.GL_TEXTURE_2D);
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }


}
