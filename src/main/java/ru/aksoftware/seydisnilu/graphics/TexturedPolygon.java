package ru.aksoftware.seydisnilu.graphics;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import ru.aksoftware.seydisnilu.misc.Helper3D;

import javax.microedition.khronos.opengles.GL10;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Депрачел on 15.02.15.
 */
public abstract class TexturedPolygon implements Drawable {
    private static final float[] vertices = {
            0.0f, 0.0f, 0.0f,  // 0, Top Left
            0.0f, -1.0f, 0.0f,  // 1, Bottom Left
            1.0f, 0.0f, 0.0f,  // 2, Top Right
            1.0f, -1.0f, 0.0f,  // 3, Bottom Right
    };
    protected FloatBuffer uvBuffer;
    protected ShortBuffer indexBuffer;
    protected FloatBuffer vertexBuffer;
    int[] texture = new int[1];

    protected void initBuffers() {
        vertexBuffer = Helper3D.arrayToBuffer(vertices);
        float[] uvCoords = {0, 0, 0, 1, 1, 0, 1, 1};
        uvBuffer = Helper3D.arrayToBuffer(uvCoords);
        indexBuffer = Helper3D.createSequentialBuffer(4);
    }

    public void draw(GL10 gl) {

        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, texture[0]);

        gl.glEnable(gl.GL_BLEND);
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA);

        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

        //gl.glColor4f(0.0f, 0.5f, 0.5f, 0.8f);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, uvBuffer);
        gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, 5,
                GL10.GL_UNSIGNED_SHORT, indexBuffer);
        gl.glDisable(gl.GL_BLEND);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisable(GL10.GL_TEXTURE_2D);

    }

    @Override
    public void init(GL10 gl) throws IOException {
        initTexture(gl);
    }

    public void initTexture(GL10 gl) throws IOException {
        //Generate one texture pointer...
        gl.glGenTextures(1, texture, 0);
        redrawBitmap(gl);
    }

    protected void bitmapToTexture(GL10 gl, Bitmap bitmap) {
        gl.glBindTexture(GL10.GL_TEXTURE_2D, texture[0]);

        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

        bitmap.recycle();
    }

    public abstract void redrawBitmap(GL10 gl) throws IOException;

    @Override
    public void refresh(GL10 gl) throws IOException {
        redrawBitmap(gl);
    }

}
