package ru.aksoftware.seydisnilu.graphics;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

import ru.aksoftware.seydisnilu.misc.FColor;
import ru.aksoftware.seydisnilu.misc.Helper3D;

/**
 * Created by kabk on 30.12.2014.
 */
public class Trinangle {

    FloatBuffer vertexBuffer;
    ShortBuffer indexBuffer;
    FColor color = new FColor(1, 1, 1);

    public void setColor(FColor color) {
        this.color = color;
    }

    public Trinangle(float... coordinates) {
        if(coordinates.length < 9) {
            throw new IllegalArgumentException("Trinangle coordinates size can not be less then 9");
        }
        vertexBuffer = Helper3D.arrayToBuffer(coordinates);
        indexBuffer = Helper3D.createSequentialBuffer(3);
    }

    public void draw(GL10 gl) {

        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

        gl.glColor4f(color.red(), color.green(), color.blue(), color.alpha());
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, 3,
                GL10.GL_UNSIGNED_SHORT, indexBuffer);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    }


}
