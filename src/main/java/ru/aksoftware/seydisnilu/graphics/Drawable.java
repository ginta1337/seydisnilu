package ru.aksoftware.seydisnilu.graphics;

import javax.microedition.khronos.opengles.GL10;
import java.io.IOException;

/**
 * Created by Депрачел on 15.02.15.
 */
public interface Drawable {

    public void draw(GL10 gl);

    public void init(GL10 gl) throws IOException;

    public void refresh(GL10 gl) throws IOException;
}
