package ru.aksoftware.seydisnilu.graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.opengl.GLUtils;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

import ru.aksoftware.seydisnilu.misc.FColor;
import ru.aksoftware.seydisnilu.misc.Helper3D;

/**
 * Created by kabk on 27.12.2014.
 */
public class TextShape extends TexturedPolygon  {

    private String text;
    private int textSize = 32;
    private int iWidth;
    private int iHeight;

    private int fontSize;

    // The order we like to connect them.
    //private static final short[] indices = { 0, 1, 2, 0, 2, 3 };


    public TextShape(String text) {
        //text.length()*16
        this(256, 45, text);
    }

    private FColor color = new FColor(1.0f, 1.0f, 1.0f);
    private int foneColor = Color.argb(0, 0, 0, 0);

    public void setTextColor(FColor color) {
        if(color == null)
            return;
        this.color = color;
    }

    public static int calculateTextWidth(String text, int fontSize) {
        return (int) (text.length()*fontSize*0.75f);
    }

    public TextShape(int iWidth, int iHeight, String text) {
        this.iWidth = iWidth;
        this.iHeight = iHeight;
        this.text = text;
        initBuffers();
    }

    public TextShape(int fontSize, String text) {
        this(calculateTextWidth(text, fontSize), fontSize*2, text);
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setFoneColor(int foneColor) {
        this.foneColor = foneColor;
    }

    public void redrawBitmap(GL10 gl) {
        Bitmap bitmap = Bitmap.createBitmap(iWidth, iHeight, Bitmap.Config.ARGB_4444);
        // get a canvas to paint over the bitmap
        Canvas canvas = new Canvas(bitmap);
        bitmap.eraseColor(foneColor);


// get a background image from resources
// note the image format must match the bitmap format
//        Drawable background = context.getResources().getDrawable(R.drawable.background);
//        background.setBounds(0, 0, 256, 256);
//        background.draw(canvas); // draw the background to our bitmap

// Draw the text
        Paint textPaint = new Paint();
        textSize = 32;
        textPaint.setTextSize(textSize);
        textPaint.setAntiAlias(true);
        textPaint.setARGB(0xff, (int) (255 * color.red()), (int) (255 * color.green()), (int) (255 * color.blue()));
// draw the text centered
        canvas.drawText(text, fontSize/2, 3 * iHeight / 4, textPaint);
        //...and bind it to our array
        bitmapToTexture(gl, bitmap);
    }

}
