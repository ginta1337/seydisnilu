package ru.aksoftware.seydisnilu.graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Cylinder {

    private final float heigth;
    private final int length;
    private final int sections;
    private float[] bottomCenterPoint;
    private float[][] vertices;
    private short[] indices;
    public static final float PI = 3.1415f;

    private FloatBuffer[] vertexBufferSides;
    private FloatBuffer vertexBufferTop;
    private FloatBuffer vertexBufferBottom;

    private ShortBuffer indexBuffer;

    private boolean closed;
    private float[] centerPoint;

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Cylinder(int sections, int length, float heigth, float radius) {
        this.sections = sections;
        this.length = length;
        this.heigth = heigth;

        calcuatePoints(sections, length, heigth, radius);

        prepareBuffers(length);

    }

    private void prepareBuffers(int length) {
        vertexBufferSides = new FloatBuffer[length];
        // a float is 4 bytes, therefore we multiply the number if
        // vertices with 4.
        for (int i = 0; i < length; i++) {
            ByteBuffer vbb = ByteBuffer.allocateDirect(vertices[i].length * 4);
            vbb.order(ByteOrder.nativeOrder());
            vertexBufferSides[i] = vbb.asFloatBuffer();
            vertexBufferSides[i].put(vertices[i]);
            vertexBufferSides[i].position(0);
        }

        vertexBufferTop = prepareFlatSide(centerPoint, 0);
        vertexBufferBottom = prepareFlatSide(bottomCenterPoint, length - 1);

        // short is 2 bytes, therefore we multiply the number if
        // vertices with 2.
        ByteBuffer ibb = ByteBuffer.allocateDirect(indices.length * 2);
        ibb.order(ByteOrder.nativeOrder());
        indexBuffer = ibb.asShortBuffer();
        indexBuffer.put(indices);
        indexBuffer.position(0);
    }

    private FloatBuffer prepareFlatSide(float[] centerPoint, int i1) {
        ByteBuffer topBB = ByteBuffer.allocateDirect((length + 2)*3*4);
        topBB.order(ByteOrder.nativeOrder());
        FloatBuffer floatBuffer = topBB.asFloatBuffer();

        floatBuffer.put(centerPoint, 0, 3);
        for(int i = 0; i < length; i++) {
            int verticeIndex = i * 2 * 3;
            floatBuffer.put(vertices[i1], verticeIndex, 3);
        }
        floatBuffer.position(0);
        return floatBuffer;
    }

    private void calcuatePoints(int sections, int length, float heigth, float radius) {
        centerPoint = new float[]{0,0,0};
        bottomCenterPoint = new float[]{0,0,length*heigth};

        vertices = new float[length][(sections+1) * 2 * 3];
        float alpha = (2.0f * PI) / (float) sections;
        for (int i = 0; i < sections+1; i++) {
            float x = (float) Math.cos(alpha * i) * radius;
            float y = (float) Math.sin(alpha * i) * radius;
            int verticeIndex = i * 2 * 3;
            for (int j = 0; j < length; j++) {
                vertices[j][verticeIndex] = x;
                vertices[j][verticeIndex + 1] = y;
                vertices[j][verticeIndex + 2] = (float) j * (heigth);
                vertices[j][verticeIndex + 3] = x;
                vertices[j][verticeIndex + 4] = y;
                vertices[j][verticeIndex + 5] = (float) (j + 1) * (heigth);
            }
        }


        indices = new short[(sections+1) * 2];
        for (short i = 0; i < indices.length; i++) {
            indices[i] = i;
        }
    }

    public void draw(GL10 gl) {

        // Clears the screen and depth buffer.
//        gl.glClear(GL10.GL_COLOR_BUFFER_BIT |
//                GL10.GL_DEPTH_BUFFER_BIT);
        // Counter-clockwise winding.
        gl.glFrontFace(GL10.GL_CCW);
        // Enable face culling.
         gl.glEnable(GL10.GL_CULL_FACE);
        // What faces to remove with the face culling.
         gl.glCullFace(GL10.GL_FRONT_AND_BACK);

        // Enabled the vertices buffer for writing and to be used during
        // rendering.
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        // Specifies the location and data format of an array of vertex
        // coordinates to use when rendering.
        int cLength = vertices.length;
        for (int i = 0; i < cLength; i++) {
            float delta = 1.0f / cLength;
            gl.glColor4f(delta * (cLength - i), 0.0f, delta * i, 0.8f);
            //gl.glColor4f(0.6f, 0.6f, 0.6f, 0.8f);
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBufferSides[i]);
            gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, indices.length,
                    GL10.GL_UNSIGNED_SHORT, indexBuffer);
        }

/*        gl.glColor4f(1.0f, 0.0f, 0, 0.8f);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBufferTop);
        gl.glDrawElements(GL10.GL_LINE_STRIP, 3,
                GL10.GL_UNSIGNED_SHORT, indexBuffer);*/

        gl.glColor4f(1.0f, 0.0f, 0, 0.8f);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBufferTop);
        gl.glDrawElements(GL10.GL_TRIANGLE_FAN, length + 2,
                GL10.GL_UNSIGNED_SHORT, indexBuffer);

        gl.glColor4f(0.0f, 0.0f, 1.0f, 0.8f);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBufferBottom);
        gl.glDrawElements(GL10.GL_TRIANGLE_FAN, length + 2,
                GL10.GL_UNSIGNED_SHORT, indexBuffer);

/*
        drawPlate(gl, cLength, 0, vbb, centerPoint);

        drawPlate(gl, cLength, length - 1, vbb, bottomCenterPoint);
*/


        // Disable the vertices buffer.
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        // Disable face culling.
        gl.glDisable(GL10.GL_CULL_FACE);
    }

/*
    private void drawPlate(GL10 gl, int cLength, int i1, FloatBuffer vbb, float[] centerPoint) {
        vbb.put(centerPoint, 0, 3);
        for(int i = 0; i < cLength / 2; i++) {
            vbb.put(3, vertices[i1][i*2]);
            vbb.put(4, vertices[i1][i*2 + 1]);
            vbb.put(5, vertices[i1][i*2 + 2]);
            vbb.put(6, vertices[i1][i*2 + 6]);
            vbb.put(7, vertices[i1][i*2 + 7]);
            vbb.put(8, vertices[i1][i*2 + 8]);
            gl.glColor4f(1f, 0.6f, 0.6f, 0.8f);
            gl.glVertexPointer(3, GL10.GL_FLOAT, i1, vbb);
            gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, 9,
                    GL10.GL_UNSIGNED_SHORT, indexBuffer);
        }
    }
*/

}
