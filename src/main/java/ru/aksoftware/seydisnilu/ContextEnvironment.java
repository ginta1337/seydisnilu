package ru.aksoftware.seydisnilu;

import android.content.Context;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by kabk on 30.12.2014.
 */
public interface ContextEnvironment {

//  GL10 getGl();

    Context getContext();

    void playVideo(int resourceId);

}
