package ru.aksoftware.seydisnilu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class RadialTetView extends View {

	public RadialTetView(Context context) {
		super(context);
	}

    public RadialTetView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RadialTetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
