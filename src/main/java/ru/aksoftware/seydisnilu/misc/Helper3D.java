package ru.aksoftware.seydisnilu.misc;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by kabk on 27.12.2014.
 */
public class Helper3D {

    public static ShortBuffer arrayToBuffer(short[] array) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(array.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
        shortBuffer.put(array);
        shortBuffer.position(0);
        return shortBuffer;
    }

    public static FloatBuffer arrayToBuffer(float[] array) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(array.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
        floatBuffer.put(array);
        floatBuffer.position(0);
        return floatBuffer;
    }

    public static ShortBuffer createSequentialBuffer(int size) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(size*2);
        short[] indices = new short[size];
        for (short i = 0; i < size; i++) {
            indices[i] = i;
        }


        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
        shortBuffer.put(indices);
        shortBuffer.position(0);
        return shortBuffer;
    }

}
