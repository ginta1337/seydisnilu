package ru.aksoftware.seydisnilu.misc;

/**
 * Created by Депрачел on 16.07.14.
 */
public class FColor {
    private final float[] color;

    public FColor(float[] color) {
        this.color = color;
    }

    public FColor(float red, float green, float blue) {
        this(red, green, blue, 1.0f);
    }

    public FColor(float red, float green, float blue, float alpha) {
        this.color = new float[4];
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
    }

    public float red() {
        return color[0];
    }

    public float green() {
        return color[1];
    }

    public float blue() {
        return color[2];
    }

    public float alpha() {return color[3];}
}
