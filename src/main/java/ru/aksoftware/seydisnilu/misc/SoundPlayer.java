package ru.aksoftware.seydisnilu.misc;

import android.content.Context;
import android.media.MediaPlayer;

import ru.aksoftware.seydisnilu.ApplicationState;
import ru.aksoftware.seydisnilu.R;

/**
 * Created by Депрачел on 16.02.15.
 */
public class SoundPlayer {

    private MediaPlayer mediaPlayer;


    boolean paused = false;


    public void playSound(int resourceId) {
        playSound(resourceId, false);
    }

    public void playSound(int resourceId, boolean looped) {
        mediaPlayer = MediaPlayer.create(ApplicationState.getInstance().getContextEnvironment().getContext(), resourceId);
        mediaPlayer.setLooping(looped);
        if (looped == false) {
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.release();
                }
            });
        }
        mediaPlayer.start();
        paused = false;
    }

    public void pause() {

        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                paused = true;
            }
        } catch (IllegalStateException ex) {
            //Do nothing. Media player bug stub.
        }
    }

    public void resume() {
        if (paused) {
            mediaPlayer.start();
            paused = false;
        }
    }

    public void stop() {
        paused = false;
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
        }
    }


}
