package ru.aksoftware.seydisnilu.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import ru.aksoftware.seydisnilu.ApplicationState;
import ru.aksoftware.seydisnilu.Constants;
import ru.aksoftware.seydisnilu.ContextEnvironment;
import ru.aksoftware.seydisnilu.OpenGLRenderer;
import ru.aksoftware.seydisnilu.R;
import ru.aksoftware.seydisnilu.game.BarabanContent;
import ru.aksoftware.seydisnilu.game.Game;
import ru.aksoftware.seydisnilu.game.SimpleTextSector;
import ru.aksoftware.seydisnilu.misc.FColor;

public class MainActivity extends Activity implements ContextEnvironment {

    private OpenGLRenderer renderer;
    private Context context;

    private static boolean firstLaunch = true;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.LOG_TAG, "MainActivity created");
		super.onCreate(savedInstanceState);
        ApplicationState.getInstance().init(this);

        if(firstLaunch) {
            Log.i(Constants.LOG_TAG, "Starting game");
            firstLaunch = false;
            playVideo(R.raw.intro);
        }

          this.requestWindowFeature(Window.FEATURE_NO_TITLE);
          getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                  WindowManager.LayoutParams.FLAG_FULLSCREEN);
          GLSurfaceView view = new GLSurfaceView(this);
        //ApplicationState.getInstance().init(getResources());
        context = view.getContext();
          try {
              renderer =  new OpenGLRenderer(getResources(), ApplicationState.getInstance().getGame());
              renderer.init(context);
          view.setRenderer(renderer);
          view.setOnTouchListener(renderer);
          setContentView(view);
              //new SoundPlayer(context).playSound(R.raw.intro);
          } catch (Exception ex) {
              Log.e("RadialTet", "Error during initialization", ex);
          }

	}

    @Override
    protected void onPause() {
        super.onPause();
        ApplicationState.getInstance().getGame().pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApplicationState.getInstance().getGame().resume();
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void playVideo(int resourceId) {
        Intent i = new Intent(MainActivity.this, VideoActivity.class);
        i.putExtra(VideoActivity.PARAM_VIDEO_RESOURCE, resourceId);
        startActivity(i);
    }
}
