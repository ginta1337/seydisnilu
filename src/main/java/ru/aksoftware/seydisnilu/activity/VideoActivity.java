package ru.aksoftware.seydisnilu.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.VideoView;

import ru.aksoftware.seydisnilu.Constants;
import ru.aksoftware.seydisnilu.R;

/**
 * Created by kabk on 21.02.2015.
 */
public class VideoActivity extends Activity /*implements SurfaceHolder.Callback */ {


    public static final String PARAM_POSITION = "ru.aksoftware.seydisnilu.position";
    public static final String PARAM_VIDEO_RESOURCE = "ru.aksoftware.seydisnilu.video_resource";
    public static final int DEFAULT_VIDEO = R.raw.intro;
    int newPosition = 0;
    private int videoResource = DEFAULT_VIDEO;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.video_activity_layout);

        //Button buttonPlayVideo = (Button) findViewById(R.id.btn_skip_video);

        getWindow().setFormat(PixelFormat.UNKNOWN);

        //Displays a video file.
        final VideoView mVideoView = (VideoView) findViewById(R.id.video_view);




        Intent intent = getIntent();
        if (savedInstanceState != null) {
            newPosition = savedInstanceState.getInt(PARAM_POSITION);
            videoResource = savedInstanceState.getInt(PARAM_VIDEO_RESOURCE, DEFAULT_VIDEO);
        } else if (intent != null) {
            videoResource = intent.getIntExtra(PARAM_VIDEO_RESOURCE, DEFAULT_VIDEO);
        }

        View layout = findViewById(R.id.video_layout);
        layout.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        playVideo(mVideoView);
        Log.i(Constants.LOG_TAG,
                "Playing video. Vid: " + videoResource + " Position: " + newPosition);
    }

    private void playVideo(VideoView mVideoView) {
        String uriPath = "android.resource://ru.aksoftware.seydisnilu/" + videoResource;
        Uri uri = Uri.parse(uriPath);
        mVideoView.setVideoURI(uri);

        mVideoView.requestFocus();
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                finish();
            }
        });
        mVideoView.start();
        mVideoView.seekTo(newPosition);

    }

/*
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    }
*/


    protected void onPause() {
        super.onPause();
        VideoView mVideoView = (VideoView) findViewById(R.id.video_view);
        mVideoView.pause();
    }

    protected void onResume() {
        super.onResume();
        VideoView mVideoView = (VideoView) findViewById(R.id.video_view);
        mVideoView.resume();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        VideoView mVideoView = (VideoView) findViewById(R.id.video_view);
        savedInstanceState.putInt(PARAM_POSITION, mVideoView.getCurrentPosition());
        savedInstanceState.putInt(PARAM_VIDEO_RESOURCE, videoResource);
    }
}
