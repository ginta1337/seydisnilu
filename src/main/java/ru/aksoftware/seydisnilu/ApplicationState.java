package ru.aksoftware.seydisnilu;

import android.content.res.Resources;

import ru.aksoftware.seydisnilu.game.BarabanContent;
import ru.aksoftware.seydisnilu.game.Game;
import ru.aksoftware.seydisnilu.game.SimpleTextSector;
import ru.aksoftware.seydisnilu.game.VideoSector;
import ru.aksoftware.seydisnilu.misc.FColor;

public class ApplicationState {

    private static final ApplicationState INSTANCE = new ApplicationState();
    private Game game;
    private ContextEnvironment contextEnvironment;

    public static ApplicationState getInstance() {
        return INSTANCE;
    }

    public void init(ContextEnvironment contextEnvironment) {
        this.contextEnvironment = contextEnvironment;
        if (game == null) {
            createGame();
        }
    }

    public ContextEnvironment getContextEnvironment() {
        return contextEnvironment;
    }

    public void createGame() {
        SimpleTextSector pzir = new VideoSector("ПЗИР", new FColor(0, 0, 0), OpenGLRenderer.RADIUS, R.raw.pzir1);
        pzir.setSectorSound(R.raw.pzir);
        BarabanContent barabanContent = new BarabanContent(
                new SimpleTextSector("мини плюха",  new FColor(0, 0, 0), OpenGLRenderer.RADIUS),
                new SimpleTextSector("норм плюха",  new FColor(0, 0, 0), OpenGLRenderer.RADIUS),
                new SimpleTextSector("мини плюха",  new FColor(0, 0, 0), OpenGLRenderer.RADIUS),
                new SimpleTextSector("большая плюха",  new FColor(0, 0, 0), OpenGLRenderer.RADIUS),
                new SimpleTextSector("мини плюха",  new FColor(0, 0, 0), OpenGLRenderer.RADIUS),
                new SimpleTextSector("норм плюха",  new FColor(0, 0, 0), OpenGLRenderer.RADIUS),
                pzir,
                new SimpleTextSector("большая плюха",  new FColor(0, 0, 0), OpenGLRenderer.RADIUS));
        game = new Game(barabanContent);
    }

    public Game getGame() {
        return game;
    }
}
