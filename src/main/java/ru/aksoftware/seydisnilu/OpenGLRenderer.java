package ru.aksoftware.seydisnilu;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import ru.aksoftware.seydisnilu.game.BarabanContent;
import ru.aksoftware.seydisnilu.game.Game;
import ru.aksoftware.seydisnilu.game.SimpleTextSector;
import ru.aksoftware.seydisnilu.graphics.*;
import ru.aksoftware.seydisnilu.misc.FColor;

import java.io.IOException;

import static javax.microedition.khronos.opengles.GL10.*;


public class OpenGLRenderer implements Renderer, View.OnTouchListener {


    private static final float ANGLE_SENS = 20;
    public static final int LIGHT_MAX_POS = 20;
    public static final int LIGHT_MIN_POS = -20;
    public static final int FRAME_RATE = 33;
    public static final float RADIUS = 2.0f;
    public static final float HEIGTH = 2.0f;

    private final Game game;
    /*3D Shapes*/
    private Baraban baraban;
    //private TextShape text;
    //Square square = new Square();
    private Trinangle trinangle;
    //private VideoShape videoShape;
    private final Resources resources;


    float angle = 0;
    View v;
    private ScaleGestureDetector mScaleDetector;
    float mScaleFactor = 1.0f;


    float lastX;
    float lastY;
    float angleX = 0;//Cylinder.PI / 4;
    float angleY = Cylinder.PI / 2;//Cylinder.PI / 4;
    float deltaX = 0f;
    float deltaY = 0f;




    private float[] lightPosition;
    private float[] lightDirection;
    private float previousAngle;
    private float angleChange;
    private Context mContext;
    private boolean gameInitialized;

    public OpenGLRenderer(Resources resources, Game game) {
        this.resources = resources;
        this.game = game;
    }


    public void init(Context context) {
        mContext = context;
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        previousAngle = angle;
        trinangle = new Trinangle(4*RADIUS/5, 0, HEIGTH+0.01f, 7*RADIUS/5, RADIUS/3, HEIGTH+0.01f, 7*RADIUS/5, -RADIUS/3, HEIGTH+0.01f);
        trinangle.setColor(new FColor(1, 0, 0));

        //videoShape = new VideoShape(R.raw.intro);


    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        // Set the background color to black ( rgba ).
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
        //gl.glEnable(GL10.GL_TEXTURE_2D);
        // Enable Smooth Shading, default not really needed.
        gl.glShadeModel(GL10.GL_SMOOTH);
        // Depth buffer setup.
        gl.glClearDepthf(1.0f);
        // Enables depth testing.
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glDepthFunc(GL10.GL_LEQUAL);
        // The type of depth testing to do.
        gl.glDepthFunc(GL10.GL_LEQUAL);
        // Really nice perspective calculations.
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
//        text = new TextShape("Hello 3D!");
//        text.setFoneColor(Color.argb(128, 0, 255, 0));
//        text.initTexture(gl);


        try {
            initGame(gl);
            baraban.prepareTextures(gl, mContext);
            game.init(gl);
        } catch (IOException e) {
            Log.e("SEYDISNILU", "Error while initializing game" ,e);
            throw new RuntimeException(e);
        }
        // gl.glEnable(GL10.GL_LIGHTING);
    }

    private void initGame(GL10 gl) throws IOException {
        if(!gameInitialized) {
            gameInitialized = true;
        } else {
            return;
        }

        baraban= new Baraban(32, HEIGTH, RADIUS, game);

    }

    boolean lightUp;

    private long startTime = System.currentTimeMillis();

    public void onDrawFrame(GL10 gl) {

        process();
        adjustFrameRate();

        // Clears the screen and depth buffer.
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | //
                GL10.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();

        drawOCHOBA(gl);
        drawMenu(gl);
    }

    private void drawOCHOBA(GL10 gl) {

        gl.glPushMatrix();
        gl.glTranslatef(0, 0, -8 * mScaleFactor);


        lightPosition[3] += lightUp ? 0.05f : -0.05f;
        if (lightPosition[3] > LIGHT_MAX_POS && lightUp) {
            lightUp = false;
        } else if (lightPosition[3] < LIGHT_MIN_POS) {
            lightUp = true;
        }

        //videoShape.draw(gl);

        gl.glRotatef(45.0f, -1, 0, 0);
        drawBaraban(gl, game.getAngle());

        trinangle.draw(gl);
        gl.glPopMatrix();
    }

    private void drawBaraban(GL10 gl, float newAngle) {
        gl.glPushMatrix();
        gl.glRotatef(newAngle, 0, 0, 1);
        baraban.draw(gl);
        gl.glPopMatrix();
    }

    private void drawMenu(GL10 gl) {
        gl.glPushMatrix();
        //gl.glTranslatef(-0.5f, -0.5f, 0);
        gl.glTranslatef(0, 0, -1);
        gl.glTranslatef(-0.2f, 0.2f, 0);
        gl.glScalef(0.2f, 0.1f, 1f);
        //square.draw(gl);
        //text.draw();
        gl.glPopMatrix();
    }

    private void adjustFrameRate() {
        long endTime = System.currentTimeMillis();
        long dt = endTime - startTime;
        if (dt < FRAME_RATE) {
            try {
                Thread.sleep(FRAME_RATE - dt);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        startTime = System.currentTimeMillis();
    }

    private void process() {
        game.process();
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        // Sets the current view port to the new size.
        gl.glViewport(0, 0, width, height);
        // Select the projection matrix
        gl.glMatrixMode(GL10.GL_PROJECTION);
        // Reset the projection matrix
        gl.glLoadIdentity();
        // Calculate the aspect ratio of the window
        GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f,
                100.0f);
        // Select the modelview matrix
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        // Reset the modelview matrix
        gl.glLoadIdentity();

        float[] ambient = {0.4f, 1, 1, 1};
        float[] specular = {1.0f, 1.0f, 1.0f, 1.0f};
        float[] diffuseLight = {0.8f, 0.8f, 0.8f, 1.0f};
        lightPosition = new float[]{0, 0, 0, 1.0f};
        lightDirection = new float[]{0, 0, 1};

        /*gl.glEnable(GL10.GL_LIGHTING);
        gl.glEnable(GL10.GL_LIGHT1);
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, ambient, 0);
        gl.glEnable(GL10.GL_LIGHT1);
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_SPECULAR, specular, 0);
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, diffuseLight, 0);
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_POSITION, lightPosition, 0);*/
        //gl.glLightf(GL10.GL_LIGHT1, GL10.GL_SPOT_CUTOFF, 30.0f);

        setMaterial(gl);
    }

    private void setMaterial(GL10 gl) {
        float shininess = 30;
        float[] ambient = {.3f, .3f, .3f, 1};
        float[] diffuse = {.3f, .3f, .3f, 1};
        float[] specular = {1, 1, 1, .5f};

        gl.glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse, 0);
        gl.glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient, 0);
        gl.glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular, 0);
        gl.glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent event) {
        mScaleDetector.onTouchEvent(event);

        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = x;
                lastY = y;
                //angleSpeed = 0;

                break;

            case MotionEvent.ACTION_MOVE:
                deltaX = (x - lastX) / ANGLE_SENS;
                deltaY = (y - lastY) / ANGLE_SENS;
                lastX = x;
                lastY = y;
                game.rotate(deltaX);
                break;

            case MotionEvent.ACTION_UP:
//                deltaX = (x - lastX) / ANGLE_SENS;
//                deltaY = (y - lastY) / ANGLE_SENS;
//                game.rotate(deltaX);
                //angle = angle + deltaX;
                game.setRotationSpeed(deltaX);
                //angleSpeed = deltaX;
//                angleX = angleX + deltaX;
//                angleY = angleY + deltaY;
                deltaX = 0;
                deltaY = 0;
                break;
        }
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor /= detector.getScaleFactor();

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));

            return true;
        }
    }

//    public void pause() {
//        game.pause();
//    }


}