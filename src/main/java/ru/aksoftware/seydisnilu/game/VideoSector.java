package ru.aksoftware.seydisnilu.game;

import ru.aksoftware.seydisnilu.ApplicationState;
import ru.aksoftware.seydisnilu.misc.FColor;

/**
 * Created by kabk on 22.02.2015.
 */
public class VideoSector extends SimpleTextSector {

    private final int videoResourceId;

    public VideoSector(String text, float radius, int videoResourceId) {
        super(text, radius);
        this.videoResourceId = videoResourceId;
    }

    public VideoSector(String text, FColor color, float radius, int videoResourceId) {
        super(text, color, radius);
        this.videoResourceId = videoResourceId;
    }

    @Override
    public SectorAction getSectorAction() {
        return new SectorAction() {
            @Override
            public void execute() {
                ApplicationState.getInstance().getContextEnvironment().playVideo(videoResourceId);
            }
        };
    }

}
