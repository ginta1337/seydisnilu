package ru.aksoftware.seydisnilu.game;

import javax.microedition.khronos.opengles.GL10;
import java.io.IOException;

/**
 * Created by kabk on 28.12.2014.
 */
public interface SectorContent {
    public void draw(GL10 gl);

    public void reinitDrawable(GL10 gl) throws IOException;

}
