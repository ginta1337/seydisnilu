package ru.aksoftware.seydisnilu.game;

import android.content.Context;

import java.io.IOException;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;

import ru.aksoftware.seydisnilu.R;
import ru.aksoftware.seydisnilu.misc.SoundPlayer;

/**
 * Created by kabk on 30.12.2014.
 */
public class Game {

    public static final float ANGLE_STOP_SPEED = 0.05f;
    public static final int RANDOM_AMOUNT = 330;

    private BarabanContent barabanContent;
    private boolean locked = false;
    private float angle;
    float rotationSpeed;

    SoundPlayer barabanSoundPlayer;

    int selectedId = -1;
    private float anglePerSector;

    public float getAngle() {
        return angle;
    }

    public void select(int selection) {

        selectedId = selection;
        barabanContent.getSectors()[selectedId].getSectorAction().execute();

    }

    public void deselect() {
        selectedId = -1;
    }

    public int getSelectedId() {
        return selectedId;
    }


    public Game(BarabanContent barabanContent) {
        this.barabanContent = barabanContent;
        anglePerSector = 360 / barabanContent.size();
        barabanSoundPlayer = new SoundPlayer();

    }


    public void rotate(float delta) {
        if (!locked) {
            angle += delta;
            deselect();
        }
    }

    private final static Random rnd = new Random();

    public void setRotationSpeed(float speed) {
        if (!locked) {
            if (speed != 0) {
                barabanSoundPlayer.playSound(R.raw.baraban, true);
                rotationSpeed = speed + ANGLE_STOP_SPEED * rnd.nextInt(RANDOM_AMOUNT)*Math.signum(speed);
                locked = true;
                deselect();
            }
        }
    }

    public BarabanContent getBarabanContent() {
        return barabanContent;
    }

    public void process() {
        //angle += Cylinder.PI / 10;
        angle += rotationSpeed;
        if (angle < 0) {
            angle += 360;
        } else if (angle > 360) {
            angle -= 360;
        }
        //selectCurrentSector();


        if (Math.abs(rotationSpeed) > 0) {

            rotationSpeed = Math.abs(rotationSpeed) < ANGLE_STOP_SPEED ? 0f :
                    (rotationSpeed > 0 ? rotationSpeed - ANGLE_STOP_SPEED : rotationSpeed + ANGLE_STOP_SPEED);
            if (rotationSpeed == 0) {
                rotationFinished();
            }
        }
    }

    private void rotationFinished() {
        barabanSoundPlayer.stop();
        selectCurrentSector();
        locked = false;
    }

    private void selectCurrentSector() {
        int invSelection = ((int) (angle / anglePerSector)) % barabanContent.size();
        int selection = barabanContent.size() - invSelection - 1;
        select(selection);
    }

    public void pause() {
        barabanSoundPlayer.pause();
    }

    public void resume() {
        barabanSoundPlayer.resume();
    }

    public void init(GL10 gl) throws IOException {
        for(Sector sector : barabanContent.getSectors()) {
            sector.getSectorContent().reinitDrawable(gl);
        }
    }
}
