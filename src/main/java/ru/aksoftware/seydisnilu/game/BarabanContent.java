package ru.aksoftware.seydisnilu.game;

import java.util.Set;

/**
 * Created by Депрачел on 13.12.14.
 */
public class BarabanContent {

    Sector[] sectors;

    public BarabanContent(Sector... sectors) {
        this.sectors = sectors;
    }

    public int size() {
        return sectors.length;
    }


    public Sector[] getSectors() {
        return sectors;
    }
}
