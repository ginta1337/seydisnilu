package ru.aksoftware.seydisnilu.game;

import android.media.MediaPlayer;

import javax.microedition.khronos.opengles.GL10;


import ru.aksoftware.seydisnilu.ContextEnvironment;
import ru.aksoftware.seydisnilu.R;
import ru.aksoftware.seydisnilu.graphics.TextShape;
import ru.aksoftware.seydisnilu.misc.FColor;
import ru.aksoftware.seydisnilu.misc.SoundPlayer;

import java.io.IOException;

/**
 * Created by kabk on 28.12.2014.
 */
public class SimpleTextSector implements Sector {


    private TextShape textShape;
    private float radius;
    private int mSectorSound = R.raw.select;
    private FColor textColor;
    private SoundPlayer player;

    public SimpleTextSector(String text, float radius) {
        this(text, new FColor(0,0,0), radius);
    }

    public SimpleTextSector(String text, FColor color, float radius ) {
        this.radius = radius;
        textColor = color;
        textShape = new TextShape(32, text);
        textShape.setTextColor(textColor);
        player = new SoundPlayer();
    }

    public void setSectorSound(int mSectorSound) {
        this.mSectorSound = mSectorSound;
    }

    public void setTextColor(FColor color) {
            textColor = color;
        if(textShape != null) {
            textShape.setTextColor(color);
        }
    }

    @Override
    public SectorAction getSectorAction() {
        return new SectorAction() {
            @Override
            public void execute() {
                player.playSound(mSectorSound);
            }
        };
    }

    @Override
    public SectorContent getSectorContent() {
        return new SectorContent() {
            @Override
            public void draw(GL10 gl) {
                gl.glPushMatrix();
                gl.glTranslatef(radius/3, radius/4, 0);
                //gl.glRotatef(-45, 0, 0, 1);
                textShape.draw(gl);
                gl.glPopMatrix();
            }

            @Override
            public void reinitDrawable(GL10 gl) throws IOException {
                textShape.initTexture(gl);
            }
        };
    }

}
