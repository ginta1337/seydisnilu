package ru.aksoftware.seydisnilu.game;

/**
 * Created by kabk on 28.12.2014.
 */
public interface SectorAction {

    public static final SectorAction NONE = new SectorAction() {
        @Override
        public void execute() {
            //Do nothing
        }
    };

    public void execute();
}
