package ru.aksoftware.seydisnilu.game;

/**
 * Created by Депрачел on 13.12.14.
 */
public interface Sector {

    public SectorAction getSectorAction();

    public SectorContent getSectorContent();


}
